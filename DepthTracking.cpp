﻿//------------------------------------------------------------------------------
// <copyright file="DepthTracking.cpp" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

// This module provides sample code used to demonstrate Kinect NUI processing

// Note: 
//     Platform SDK lib path should be added before the VC lib
//     path, because uuid.lib in VC lib path may be older

#include "stdafx.h"
#include <strsafe.h>
#include "DepthTracking.h"
#include "resource.h"
#include "guicon.h"

#include <fstream>
using namespace std;

// Global Variables:
CDepthTrackingApp  g_DepthTrackingApp;  // Application class

#define INSTANCE_MUTEX_NAME L"DepthTrackingInstanceCheck"

//-------------------------------------------------------------------
// _tWinMain
//
// Entry point for the application
//-------------------------------------------------------------------
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{

	#ifdef _DEBUG
		RedirectIOToConsole();
	#endif

	MSG       msg;
    WNDCLASS  wc;
        
    // unique mutex, if it already exists there is already an instance of this app running
    // in that case we want to show the user an error dialog
    HANDLE hMutex = CreateMutex( NULL, FALSE, INSTANCE_MUTEX_NAME );
    if ( (hMutex != NULL) && (GetLastError() == ERROR_ALREADY_EXISTS) ) 
    {
        TCHAR szAppTitle[256] = { 0 };
        TCHAR szRes[512] = { 0 };

        //load the app title
        LoadString( hInstance, IDS_APPTITLE, szAppTitle, _countof(szAppTitle) );

        //load the error string
        LoadString( hInstance, IDS_ERROR_APP_INSTANCE, szRes, _countof(szRes) );

        MessageBox( NULL, szRes, szAppTitle, MB_OK | MB_ICONHAND );

        CloseHandle(hMutex);
        return -1;
    }

    // Store the instance handle
    g_DepthTrackingApp.m_hInstance = hInstance;

    // Dialog custom window class
    ZeroMemory( &wc,sizeof(wc) );
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.cbWndExtra = DLGWINDOWEXTRA;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL,IDC_ARROW);
    wc.hIcon = LoadIcon(hInstance,MAKEINTRESOURCE(IDI_DepthTracking));
    wc.lpfnWndProc = DefDlgProc;
    wc.lpszClassName = SZ_APPDLG_WINDOW_CLASS;
    if( !RegisterClass(&wc) )
    {
        return 0;
    }

	// Default start alert
	g_DepthTrackingApp.g_DepthModuleOn = true;

    // Create main application window
    HWND hWndApp = CreateDialogParam(
        hInstance,
        MAKEINTRESOURCE(IDD_APP),
        NULL,
        (DLGPROC) CDepthTrackingApp::MessageRouter, 
        reinterpret_cast<LPARAM>(&g_DepthTrackingApp));

    // Show window
    ShowWindow(hWndApp,nCmdShow); 

    // Main message loop:
    while( GetMessage( &msg, NULL, 0, 0 ) ) 
    {
        // If a dialog message will be taken care of by the dialog proc
        if ( (hWndApp != NULL) && IsDialogMessage(hWndApp, &msg) )
        {
            continue;
        }

        // otherwise do our window processing
        TranslateMessage(&msg);
		DispatchMessage(&msg);
    }

	CloseHandle(hMutex);
    return static_cast<int>(msg.wParam);
}

//-------------------------------------------------------------------
// Constructor
//-------------------------------------------------------------------
CDepthTrackingApp::CDepthTrackingApp() : m_hInstance(NULL)
{
    ZeroMemory(m_szAppTitle, sizeof(m_szAppTitle));
    LoadString(m_hInstance, IDS_APPTITLE, m_szAppTitle, _countof(m_szAppTitle));

    m_fUpdatingUi = false;
    Nui_Zero();

    // Init Direct2D
    D2D1CreateFactory( D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2DFactory );
}

//-------------------------------------------------------------------
// Destructor
//-------------------------------------------------------------------
CDepthTrackingApp::~CDepthTrackingApp()
{
    // Clean up Direct2D
    SafeRelease( m_pD2DFactory );

    Nui_Zero();
    SysFreeString(m_instanceId);
}

void CDepthTrackingApp::ClearComboBox()
{
    for ( long i = 0; i < SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_GETCOUNT, 0, 0); i++ )
    {
        SysFreeString( reinterpret_cast<BSTR>( SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_GETITEMDATA, i, 0) ) );
    }
	SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_RESETCONTENT, 0, 0);
	SendDlgItemMessage(m_hWnd, IDC_CHECKONOFF, BM_SETCHECK, 0, 0);
}

void CDepthTrackingApp::UpdateComboBox()
{
    m_fUpdatingUi = true;
    ClearComboBox();

    int numDevices = 0;
    HRESULT hr = NuiGetSensorCount(&numDevices);

    if ( FAILED(hr) )
    {
        return;
    }

    EnableWindow(GetDlgItem(m_hWnd, IDC_APPTRACKING), numDevices > 0);

    long selectedIndex = 0;
    for ( int i = 0; i < numDevices; i++ )
    {
        INuiSensor *pNui = NULL;
        HRESULT hr = NuiCreateSensorByIndex(i,  &pNui);
        if (SUCCEEDED(hr))
        {
            HRESULT status = pNui ? pNui->NuiStatus() : E_NUI_NOTCONNECTED;
            if (status == E_NUI_NOTCONNECTED)
            {
                pNui->Release();
                continue;
            }
            
            WCHAR kinectName[MAX_PATH];
            StringCchPrintfW( kinectName, _countof(kinectName), L"Kinect %d", i);
            long index = static_cast<long>( SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(kinectName)) );
            SendDlgItemMessage( m_hWnd, IDC_CAMERAS, CB_SETITEMDATA, index, reinterpret_cast<LPARAM>(pNui->NuiUniqueId()) );
            if (m_pNuiSensor && pNui == m_pNuiSensor)
            {
                selectedIndex = index;
            }
            pNui->Release();
        }
    }

    SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_SETCURSEL, selectedIndex, 0);
	SendDlgItemMessage(m_hWnd, IDC_CHECKONOFF, BM_SETCHECK, g_DepthModuleOn, 0);

    m_fUpdatingUi = false;
}

LRESULT CALLBACK CDepthTrackingApp::MessageRouter( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    CDepthTrackingApp *pThis = NULL;
    
    if ( WM_INITDIALOG == uMsg )
    {
        pThis = reinterpret_cast<CDepthTrackingApp*>(lParam);
        SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis));
        NuiSetDeviceStatusCallback( &CDepthTrackingApp::Nui_StatusProcThunk, pThis );
    }
    else
    {
        pThis = reinterpret_cast<CDepthTrackingApp*>(::GetWindowLongPtr(hwnd, GWLP_USERDATA));
    }

    if ( NULL != pThis )
    {
        return pThis->WndProc( hwnd, uMsg, wParam, lParam );
    }

    return 0;
}

//-------------------------------------------------------------------
// WndProc
//
// Handle windows messages
//-------------------------------------------------------------------
LRESULT CALLBACK CDepthTrackingApp::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch(message)
    {
        case WM_INITDIALOG:
        {
            LOGFONT lf;

            // Clean state the class
            Nui_Zero();

			ofstream outFile;
			outFile.open("DEBUG.txt", ios::out | ios::trunc);
			outFile.close();

			#ifdef _RECORDS
				outFile.open("Debug\/DEPTH_TRACKING.txt", ios::out | ios::trunc);
				outFile.close();
				outFile.open("Debug\/DEPTH_TRACKING_LAST.txt", ios::out | ios::trunc);
				outFile.close();
				outFile.open("Debug\/DEPTH_TRACKING_TREND.txt", ios::out | ios::trunc);
				outFile.close();
			#endif

            // Bind application window handle
            m_hWnd = hWnd;

            // Set the font for Frames Per Second display
            GetObject( (HFONT) GetStockObject(DEFAULT_GUI_FONT), sizeof(lf), &lf );
            lf.lfHeight *= 4;
            m_hFontFPS = CreateFontIndirect(&lf);
            SendDlgItemMessage( hWnd, IDC_FPS, WM_SETFONT, (WPARAM) m_hFontFPS, 0 );

            UpdateComboBox();
            SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_SETCURSEL, 0, 0);

			// Set depth value display
			GetObject((HFONT)GetStockObject(DEFAULT_GUI_FONT), sizeof(lf), &lf);
			m_hFontDepth = CreateFontIndirect(&lf);
			SendDlgItemMessage(hWnd, IDC_DEPTH, WM_SETFONT, (WPARAM)m_hFontDepth, 0);
			m_hFontDepthTopLeftMin = CreateFontIndirect(&lf);
			m_hFontDepthTopLeftMax = CreateFontIndirect(&lf);
			m_hFontDepthTopLeftMean = CreateFontIndirect(&lf);
			m_hFontDepthTopCenterMin = CreateFontIndirect(&lf);
			m_hFontDepthTopCenterMax = CreateFontIndirect(&lf);
			m_hFontDepthTopCenterMean = CreateFontIndirect(&lf);
			m_hFontDepthTopRightMin = CreateFontIndirect(&lf);
			m_hFontDepthTopRightMax = CreateFontIndirect(&lf);
			m_hFontDepthTopRightMean = CreateFontIndirect(&lf);
			m_hFontDepthCenterLeftMin = CreateFontIndirect(&lf);
			m_hFontDepthCenterLeftMax = CreateFontIndirect(&lf);
			m_hFontDepthCenterLeftMean = CreateFontIndirect(&lf);
			m_hFontDepthCenterCenterMin = CreateFontIndirect(&lf);
			m_hFontDepthCenterCenterMax = CreateFontIndirect(&lf);
			m_hFontDepthCenterCenterMean = CreateFontIndirect(&lf);
			m_hFontDepthCenterRightMin = CreateFontIndirect(&lf);
			m_hFontDepthCenterRightMax = CreateFontIndirect(&lf);
			m_hFontDepthCenterRightMean = CreateFontIndirect(&lf);
			m_hFontDepthBottomLeftMin = CreateFontIndirect(&lf);
			m_hFontDepthBottomLeftMax = CreateFontIndirect(&lf);
			m_hFontDepthBottomLeftMean = CreateFontIndirect(&lf);
			m_hFontDepthBottomCenterMin = CreateFontIndirect(&lf);
			m_hFontDepthBottomCenterMax = CreateFontIndirect(&lf);
			m_hFontDepthBottomCenterMean = CreateFontIndirect(&lf);
			m_hFontDepthBottomRightMin = CreateFontIndirect(&lf);
			m_hFontDepthBottomRightMax = CreateFontIndirect(&lf);
			m_hFontDepthBottomRightMean = CreateFontIndirect(&lf);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPLEFT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthTopLeftMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPLEFT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthTopLeftMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPLEFT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthTopLeftMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPCENTER_MIN, WM_SETFONT, (WPARAM)m_hFontDepthTopCenterMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPCENTER_MAX, WM_SETFONT, (WPARAM)m_hFontDepthTopCenterMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPCENTER_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthTopCenterMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPRIGHT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthTopRightMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPRIGHT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthTopRightMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_TOPRIGHT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthTopRightMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERLEFT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthCenterLeftMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERLEFT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthCenterLeftMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERLEFT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthCenterLeftMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERCENTER_MIN, WM_SETFONT, (WPARAM)m_hFontDepthCenterCenterMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERCENTER_MAX, WM_SETFONT, (WPARAM)m_hFontDepthCenterCenterMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERCENTER_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthCenterCenterMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERRIGHT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthCenterRightMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERRIGHT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthCenterRightMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_CENTERRIGHT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthCenterRightMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMLEFT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthBottomLeftMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMLEFT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthBottomLeftMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMLEFT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthBottomLeftMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMCENTER_MIN, WM_SETFONT, (WPARAM)m_hFontDepthBottomCenterMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMCENTER_MAX, WM_SETFONT, (WPARAM)m_hFontDepthBottomCenterMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMCENTER_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthBottomCenterMean, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMRIGHT_MIN, WM_SETFONT, (WPARAM)m_hFontDepthBottomRightMin, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMRIGHT_MAX, WM_SETFONT, (WPARAM)m_hFontDepthBottomRightMax, 0);
			SendDlgItemMessage(hWnd, IDC_DEPTH_BOTTOMRIGHT_MEAN, WM_SETFONT, (WPARAM)m_hFontDepthBottomRightMean, 0);

            // Initialize and start NUI processing
            Nui_Init();
        }
        break;

        case WM_USER_UPDATE_FPS:
        {
            ::SetDlgItemInt( m_hWnd, static_cast<int>(wParam), static_cast<int>(lParam), FALSE );
        }
        break;

		case WM_UPDATE_ANGLE:
		{
			::SetDlgItemInt( m_hWnd, static_cast<int>(wParam), static_cast<int>(lParam), TRUE );
		}
		break;

		case WM_USER_UPDATE_DEPTH:
		{
			LPCTSTR lpszString = (LPCTSTR)lParam;
			::SetDlgItemText(m_hWnd, static_cast<int>(wParam), lpszString);
        }
        break;

		case WM_USER_UPDATE_DEPTH_TOPLEFT_MIN:
		case WM_USER_UPDATE_DEPTH_TOPLEFT_MAX:
		case WM_USER_UPDATE_DEPTH_TOPLEFT_MEAN:
		case WM_USER_UPDATE_DEPTH_TOPCENTER_MIN:
		case WM_USER_UPDATE_DEPTH_TOPCENTER_MAX:
		case WM_USER_UPDATE_DEPTH_TOPCENTER_MEAN:
		case WM_USER_UPDATE_DEPTH_TOPRIGHT_MIN:
		case WM_USER_UPDATE_DEPTH_TOPRIGHT_MAX:
		case WM_USER_UPDATE_DEPTH_TOPRIGHT_MEAN:
		case WM_USER_UPDATE_DEPTH_CENTERLEFT_MIN:
		case WM_USER_UPDATE_DEPTH_CENTERLEFT_MAX:
		case WM_USER_UPDATE_DEPTH_CENTERLEFT_MEAN:
		case WM_USER_UPDATE_DEPTH_CENTERCENTER_MIN:
		case WM_USER_UPDATE_DEPTH_CENTERCENTER_MAX:
		case WM_USER_UPDATE_DEPTH_CENTERCENTER_MEAN:
		case WM_USER_UPDATE_DEPTH_CENTERRIGHT_MIN:
		case WM_USER_UPDATE_DEPTH_CENTERRIGHT_MAX:
		case WM_USER_UPDATE_DEPTH_CENTERRIGHT_MEAN:
		case WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MIN:
		case WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MAX:
		case WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MEAN:
		case WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MIN:
		case WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MAX:
		case WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MEAN:
		case WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MIN:
		case WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MAX:
		case WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MEAN:
		{
			::SetDlgItemInt(m_hWnd, static_cast<int>(wParam), static_cast<int>(lParam), TRUE);
        }
        break;

        case WM_USER_UPDATE_COMBO:
        {
            UpdateComboBox();
        }
        break;

        case WM_COMMAND:
        {
            if( HIWORD( wParam ) == CBN_SELCHANGE )
            {
                switch (LOWORD(wParam))
                {
                    case IDC_CAMERAS:
                    {
                        LRESULT index = ::SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_GETCURSEL, 0, 0);

                        // Don't reconnect as a result of updating the combo box
                        if ( !m_fUpdatingUi )
                        {
                            Nui_UnInit();
                            Nui_Zero();
                            Nui_Init(reinterpret_cast<BSTR>(::SendDlgItemMessage(m_hWnd, IDC_CAMERAS, CB_GETITEMDATA, index, 0)));
                        }
                    }
                    break;
                }
            }
            else if ( HIWORD( wParam ) == BN_CLICKED )
            {
                switch (LOWORD(wParam))
                {
					case IDC_CHECKONOFF:
						g_DepthModuleOn = (SendMessage(GetDlgItem(m_hWnd, IDC_CHECKONOFF), BM_GETCHECK, 0, 0) == BST_CHECKED);
					break;
                }
            }
        }
        break;

        // If the titlebar X is clicked destroy app
        case WM_CLOSE:
            DestroyWindow(hWnd);
        break;

		case WM_DESTROY:
		{
			// Uninitialize NUI
			Nui_UnInit();

			// Other cleanup
			ClearComboBox();
			DeleteObject(m_hFontFPS);
			DeleteObject(m_hFontDepth);
			DeleteObject(m_hFontDepthTopLeftMin);
			DeleteObject(m_hFontDepthTopLeftMax);
			DeleteObject(m_hFontDepthTopLeftMean);
			DeleteObject(m_hFontDepthTopCenterMin);
			DeleteObject(m_hFontDepthTopCenterMax);
			DeleteObject(m_hFontDepthTopCenterMean);
			DeleteObject(m_hFontDepthTopRightMin);
			DeleteObject(m_hFontDepthTopRightMax);
			DeleteObject(m_hFontDepthTopRightMean);
			DeleteObject(m_hFontDepthCenterLeftMin);
			DeleteObject(m_hFontDepthCenterLeftMax);
			DeleteObject(m_hFontDepthCenterLeftMean);
			DeleteObject(m_hFontDepthCenterCenterMin);
			DeleteObject(m_hFontDepthCenterCenterMax);
			DeleteObject(m_hFontDepthCenterCenterMean);
			DeleteObject(m_hFontDepthCenterRightMin);
			DeleteObject(m_hFontDepthCenterRightMax);
			DeleteObject(m_hFontDepthCenterRightMean);
			DeleteObject(m_hFontDepthBottomLeftMin);
			DeleteObject(m_hFontDepthBottomLeftMax);
			DeleteObject(m_hFontDepthBottomLeftMean);
			DeleteObject(m_hFontDepthBottomCenterMin);
			DeleteObject(m_hFontDepthBottomCenterMax);
			DeleteObject(m_hFontDepthBottomCenterMean);
			DeleteObject(m_hFontDepthBottomRightMin);
			DeleteObject(m_hFontDepthBottomRightMax);
			DeleteObject(m_hFontDepthBottomRightMean);

			// Quit the main message pump
			PostQuitMessage(0);
		}
        break;

		case WM_COPYDATA:
		{
			COPYDATASTRUCT* cds = reinterpret_cast< COPYDATASTRUCT* >(lParam);
			int dwData = (int)cds->dwData;
			if (dwData == 100) {
				char* lpData = reinterpret_cast< char* >(cds->lpData);
				if (strcmp(lpData, "DepthModuleOn") == 0) {
					g_DepthTrackingApp.g_DepthModuleOn = true;
				}
				else if (strcmp(lpData, "DepthModuleOff") == 0) {
					g_DepthTrackingApp.g_DepthModuleOn = false;
				}
				SendDlgItemMessage(m_hWnd, IDC_CHECKONOFF, BM_SETCHECK, g_DepthModuleOn, 0);
			}
		}
		break;
    }

    return FALSE;
}

//-------------------------------------------------------------------
// MessageBoxResource
//
// Display a MessageBox with a string table table loaded string
//-------------------------------------------------------------------
int CDepthTrackingApp::MessageBoxResource( UINT nID, UINT nType )
{
    static TCHAR szRes[512];

    LoadString( m_hInstance, nID, szRes, _countof(szRes) );
    return MessageBox( m_hWnd, szRes, m_szAppTitle, nType );
}