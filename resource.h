//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DepthTracking.rc
//
#define IDC_MYICON                      2
#define IDD_DepthTracking_DIALOG        102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_DepthTracking               107
#define IDC_DepthTracking               109
#define IDD_APP                         110
#define IDR_MAINFRAME                   128
#define IDS_APPTITLE                    129
#define IDS_ERROR_APP_INSTANCE          130
#define IDS_ERROR_DRAWDEVICE            131
#define IDS_ERROR_NUIINIT               132
#define IDS_ERROR_DEPTHSTREAM           134
#define IDS_ERROR_VIDEOSTREAM           135
#define IDS_ERROR_IN_USE                140
#define IDS_ERROR_NUICREATE             150
#define IDC_DEPTHVIEWER                 1001
#define IDC_DEPTHVIEWER2                1002
#define IDC_VIDEOVIEW                   1003
#define IDC_FPS                         1004
#define IDC_STATUS                      1005
#define IDC_DEPTH_TOPLEFT_MIN           1005
#define IDB_RECONNECT                   1007
#define IDC_DEPTH_TOPLEFT_MAX           1007
#define IDC_CAMERAS                     1008
#define IDC_APPTRACKING                 1009
#define IDC_DEPTH_TOPLEFT_MEAN          1009
#define IDC_DEPTH_TOPCENTER_MIN         1010
#define IDC_DEPTH_TOPCENTER_MAX         1011
#define IDC_DEPTH_TOPCENTER_MEAN        1012
#define IDC_ANGLE                       1013
#define IDC_DEPTH_TOPRIGHT_MIN          1013
#define IDC_DEPTH_TOPRIGHT_MAX          1014
#define IDC_DEPTH_TOPRIGHT_MEAN         1015
#define IDC_DEPTH_CENTERLEFT_MIN        1016
#define IDC_DEPTH_CENTERLEFT_MAX        1017
#define IDC_DEPTH_CENTERLEFT_MEAN       1018
#define IDC_DEPTH_CENTERCENTER_MIN      1019
#define IDC_DEPTH_CENTERCENTER_MAX      1020
#define IDC_DEPTH                       1021
#define IDC_DEPTH_CENTERCENTER_MEAN     1022
#define IDC_DEPTH_CENTERRIGHT_MIN       1023
#define IDC_DEPTH_CENTERRIGHT_MAX       1024
#define IDC_DEPTH_CENTERRIGHT_MEAN      1025
#define IDC_DEPTH_BOTTOMLEFT_MIN        1026
#define IDC_DEPTH_BOTTOMLEFT_MAX        1027
#define IDC_DEPTH_BOTTOMLEFT_MEAN       1028
#define IDC_DEPTH_BOTTOMCENTER_MIN      1029
#define IDC_DEPTH_BOTTOMCENTER_MAX      1030
#define IDC_DEPTH_BOTTOMCENTER_MEAN     1031
#define IDC_DEPTH_BOTTOMRIGHT_MIN       1032
#define IDC_DEPTH_BOTTOMRIGHT_MAX       1033
#define IDC_DEPTH_BOTTOMRIGHT_MEAN      1034
#define IDC_CHECKONOFF                  1035
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
