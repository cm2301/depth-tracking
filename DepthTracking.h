//------------------------------------------------------------------------------
// <copyright file="DepthTracking.h" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

// Declares of CDepthTrackingApp class

#pragma once

#define _RECORDS = true

#include "resource.h"

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "NuiApi.h"
#include "DrawDevice.h"

using namespace std;

#define SZ_APPDLG_WINDOW_CLASS          _T("DepthTrackingAppDlgWndClass")
#define WM_USER_UPDATE_FPS              WM_USER
#define WM_USER_UPDATE_COMBO            WM_USER+1
#define WM_USER_UPDATE_TRACKING_COMBO   WM_USER+2

#define WM_UPDATE_ANGLE                    WM_USER+3

#define WM_USER_UPDATE_DEPTH            WM_USER+4
#define WM_USER_UPDATE_DEPTH_TOPLEFT_MIN    WM_USER+5
#define WM_USER_UPDATE_DEPTH_TOPLEFT_MAX    WM_USER+6
#define WM_USER_UPDATE_DEPTH_TOPLEFT_MEAN    WM_USER+7
#define WM_USER_UPDATE_DEPTH_TOPCENTER_MIN    WM_USER+8
#define WM_USER_UPDATE_DEPTH_TOPCENTER_MAX    WM_USER+9
#define WM_USER_UPDATE_DEPTH_TOPCENTER_MEAN    WM_USER+10
#define WM_USER_UPDATE_DEPTH_TOPRIGHT_MIN    WM_USER+11
#define WM_USER_UPDATE_DEPTH_TOPRIGHT_MAX    WM_USER+12
#define WM_USER_UPDATE_DEPTH_TOPRIGHT_MEAN    WM_USER+13
#define WM_USER_UPDATE_DEPTH_CENTERLEFT_MIN    WM_USER+14
#define WM_USER_UPDATE_DEPTH_CENTERLEFT_MAX    WM_USER+15
#define WM_USER_UPDATE_DEPTH_CENTERLEFT_MEAN    WM_USER+16
#define WM_USER_UPDATE_DEPTH_CENTERCENTER_MIN    WM_USER+17
#define WM_USER_UPDATE_DEPTH_CENTERCENTER_MAX    WM_USER+18
#define WM_USER_UPDATE_DEPTH_CENTERCENTER_MEAN    WM_USER+19
#define WM_USER_UPDATE_DEPTH_CENTERRIGHT_MIN    WM_USER+20
#define WM_USER_UPDATE_DEPTH_CENTERRIGHT_MAX    WM_USER+21
#define WM_USER_UPDATE_DEPTH_CENTERRIGHT_MEAN    WM_USER+22
#define WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MIN    WM_USER+23
#define WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MAX    WM_USER+24
#define WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MEAN    WM_USER+25
#define WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MIN    WM_USER+26
#define WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MAX    WM_USER+27
#define WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MEAN    WM_USER+28
#define WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MIN    WM_USER+29
#define WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MAX    WM_USER+30
#define WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MEAN    WM_USER+31

class CDepthTrackingApp
{
public:
    CDepthTrackingApp();
    ~CDepthTrackingApp();
    HRESULT                 Nui_Init( );
    HRESULT                 Nui_Init( OLECHAR * instanceName );
    void                    Nui_UnInit( );
    void                    Nui_GotDepthAlert( );
    void                    Nui_GotColorAlert( );

    void                    Nui_Zero();

    RGBQUAD                 Nui_ShortToQuad_Depth( USHORT s );

    static LRESULT CALLBACK MessageRouter(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    LRESULT CALLBACK        WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    static void CALLBACK    Nui_StatusProcThunk(HRESULT hrStatus, const OLECHAR* instanceName, const OLECHAR* uniqueDeviceName, void* pUserData);
    void CALLBACK           Nui_StatusProc( HRESULT hrStatus, const OLECHAR* instanceName, const OLECHAR* uniqueDeviceName );

    HWND                    m_hWnd;
    HINSTANCE               m_hInstance;

    int MessageBoxResource(UINT nID, UINT nType);

    void Vector_SplitToNine(vector<USHORT> vect, DWORD frameWidth, DWORD frameHeight,
        vector<USHORT> &vect1,
        vector<USHORT> &vect2,
        vector<USHORT> &vect3,
        vector<USHORT> &vect4,
        vector<USHORT> &vect5,
        vector<USHORT> &vect6,
        vector<USHORT> &vect7,
        vector<USHORT> &vect8,
        vector<USHORT> &vect9);
    void Vector_MinMax(vector<USHORT> vect, USHORT &minValue, USHORT &maxValue);
    void Vector_Mean(vector<USHORT> vect, USHORT &meanValue);
    void DepthData_To_TextFile(string filename, string mode);
    void DepthData_Get_Trend();
	void SoundAlert_DepthTrend_Trigger(int &return_value);
	bool           g_DepthModuleOn;

private:
    void UpdateComboBox();
    void ClearComboBox();

    bool                    m_fUpdatingUi;
    TCHAR                   m_szAppTitle[256];    // Application title
    static DWORD WINAPI     Nui_ProcessThread(LPVOID pParam);
	DWORD WINAPI            Nui_ProcessThread();

    // Current kinect
    INuiSensor *            m_pNuiSensor;
    BSTR                    m_instanceId;

    // Draw devices
    DrawDevice *            m_pDrawDepth;
    DrawDevice *            m_pDrawColor;
    ID2D1Factory *          m_pD2DFactory;

    // thread handling
    HANDLE        m_hThNuiProcess;
    HANDLE        m_hEvNuiProcessStop;

    HANDLE        m_hNextDepthFrameEvent;
	HANDLE        m_hNextColorFrameEvent;
	HANDLE        m_hVoiceEvent;
    HANDLE        m_pDepthStreamHandle;
    HANDLE        m_pVideoStreamHandle;
    HFONT         m_hFontFPS;
    RGBQUAD       m_rgbWk[640*480];
    std::vector<USHORT>        m_realDepth;
    bool          m_bScreenBlanked;
    int           m_DepthFramesTotal;
    DWORD         m_LastDepthFPStime;
    int           m_LastDepthFramesTotal;
    ULONG_PTR     m_GdiplusToken;

	std::vector<USHORT>           m_depthSection[10];
	USHORT           m_depthMax[10];
	USHORT           m_depthMin[10];
	USHORT           m_depthMean[10];
	INT           m_DepthTrend[10];
	INT           m_DepthTrendOccurrences[4];
	INT           m_LastDepthTrend[10];
	USHORT           m_LastDepthMax[10];
	USHORT           m_LastDepthMin[10];
	USHORT           m_LastDepthMean[10];
	bool           bool_decrease[4];
	bool           bool_zero[4];
	bool           bool_nothing[4];

	HFONT           m_hFontDepth;
	HFONT           m_hFontDepthTopLeftMin;
	HFONT           m_hFontDepthTopLeftMax;
	HFONT           m_hFontDepthTopLeftMean;
	HFONT           m_hFontDepthTopCenterMin;
	HFONT           m_hFontDepthTopCenterMax;
	HFONT           m_hFontDepthTopCenterMean;
	HFONT           m_hFontDepthTopRightMin;
	HFONT           m_hFontDepthTopRightMax;
	HFONT           m_hFontDepthTopRightMean;
	HFONT           m_hFontDepthCenterLeftMin;
	HFONT           m_hFontDepthCenterLeftMax;
	HFONT           m_hFontDepthCenterLeftMean;
	HFONT           m_hFontDepthCenterCenterMin;
	HFONT           m_hFontDepthCenterCenterMax;
	HFONT           m_hFontDepthCenterCenterMean;
	HFONT           m_hFontDepthCenterRightMin;
	HFONT           m_hFontDepthCenterRightMax;
	HFONT           m_hFontDepthCenterRightMean;
	HFONT           m_hFontDepthBottomLeftMin;
	HFONT           m_hFontDepthBottomLeftMax;
	HFONT           m_hFontDepthBottomLeftMean;
	HFONT           m_hFontDepthBottomCenterMin;
	HFONT           m_hFontDepthBottomCenterMax;
	HFONT           m_hFontDepthBottomCenterMean;
	HFONT           m_hFontDepthBottomRightMin;
	HFONT           m_hFontDepthBottomRightMax;
	HFONT           m_hFontDepthBottomRightMean;
};



